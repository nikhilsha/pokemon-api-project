import requests as req
import bytes_decode as bd
import wavplayer as music





# Just a hashtag2

space = '      '
global logged
logged = {}


def learn_about_pokemon(name):
    response = req.get('https://pokeapi.co/api/v2/pokemon/' + name)

    if str(response) == '<Response [404]>':
        del response
        print('Could not find pokemon.')
        music.play_sound('error.wav')
    else:
        music.play_sound('pokemon.wav')
        global logged
        PD = response.json()
        print('Pokemon Name: ' + PD['name'])
        print('Types:')
        types = []
        for i in range(len(PD['types'])):
            types.append(PD['types'][i]['type']['name'])
            print(space, PD['types'][i]['type']['name'])
        abilities = []
        for i in range(len(PD['abilities'])):
            abilities.append(PD['abilities'][i]['ability']['name'])
        logged[PD['name'] + '.abilities'] = abilities
        del abilities
        print('Weight:', PD['weight'], 'Pounds')
        print('Height:', PD['height'])
        inputt = input('Would you like to download ' + PD['name'] + "'s pictures? ")
        if inputt == 'yes':
            bd.MFC(PD['name'] + 'back.png', PD['sprites']['back_default'])
            bd.MFC(PD['name'] + 'backfemale.png', (PD['sprites']['back_female']))
            bd.MFC(PD['name'] + 'shinyback.png', PD['sprites']['back_shiny'])
            bd.MFC(PD['name'] + 'shinybackfemale.png', PD['sprites']['back_shiny_female'])
            bd.MFC(PD['name'] + 'front.png', PD['sprites']['front_default'])
            bd.MFC(PD['name'] + 'frontfemale.png', PD['sprites']['front_female'])
            bd.MFC(PD['name'] + 'shinyfront.png', PD['sprites']['front_shiny'])
            bd.MFC(PD['name'] + 'shinyfrontfemale.png', PD['sprites']['front_shiny_female'])
        moves = []
        for a in range(len(PD['moves'])):
            moves.append(PD['moves'][a]['move']['name'])
        logged[PD['name'] + '.moves'] = moves
        del moves
        print('Abilities logged at', PD['name'] + '.abilities')
        if inputt == 'yes':
            print('Open whatever app you use to store your files and search', PD['name'] + '. All downloaded images should appear.')
        print('Moves logged at', PD['name'] + '.moves')
        type_view = input('Would you like to view type data for this pokemon? ')
        if type_view == 'yes':
            for i in range(len(types)):
                type_data = req.get('https://pokeapi.co/api/v2/type/' + types[i]).json()

                print(types[i], 'type moves are strong against:')
                for b in range(len(type_data['damage_relations']['double_damage_to'])):
                    print(space, type_data['damage_relations']['double_damage_to'][b]['name'], 'types')

                print(types[i], 'types are weak against:')
                for c in range(len(type_data['damage_relations']['double_damage_from'])):
                    print(space, type_data['damage_relations']['double_damage_from'][c]['name'], 'type moves')

                print(types[i], "type moves are useless against:")
                for d in range(len(type_data['damage_relations']['no_damage_to'])):
                    print(space, type_data['damage_relations']['no_damage_to'][d]['name'], 'types')

                print(types[i], "types are immune to:")
                for e in range(len(type_data['damage_relations']['no_damage_from'])):
                    print(space, type_data['damage_relations']['no_damage_from'][e]['name'], 'type moves')

def learn_about_types(name):
    response = req.get('https://pokeapi.co/api/v2/type/' + name)
    if str(response) == '<Response [404]>':
        print('Pokemon type not found.')
        music.play_sound('error.wav')
    else:
        music.play_sound('pokemon.wav')
        type_data = req.get('https://pokeapi.co/api/v2/type/' + name).json()

        print(name, 'type moves are strong against:')
        for b in range(len(type_data['damage_relations']['double_damage_to'])):
            print(space, type_data['damage_relations']
                    ['double_damage_to'][b]['name'], 'types')

        print(name, 'types are weak against:')
        for c in range(len(type_data['damage_relations']['double_damage_from'])):
            print(space, type_data['damage_relations']
                    ['double_damage_from'][c]['name'], 'type moves')

        print(name, "type moves are useless against:")
        for d in range(len(type_data['damage_relations']['no_damage_to'])):
            print(space, type_data['damage_relations']
                    ['no_damage_to'][d]['name'], 'types')

        print(name, "types are immune to:")
        for e in range(len(type_data['damage_relations']['no_damage_from'])):
            print(space, type_data['damage_relations']
                    ['no_damage_from'][e]['name'], 'type moves')
