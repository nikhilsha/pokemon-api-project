# Pokemon API Project

On the GitLab Page in the "Last Commit" column, some of the files may not be from the latest version. This is because that version is the version of only that file,

Welcome to the Pokemon API Python Project (also called "pokeapi" or "pokeapi.py").

How to use:

This program is run in the file "outershell.py"
When you run the program it will ask you what you want to leanr about. Type "pokemon" if you want to learn about pokemon. Type "types" if you want to learn about a specific pokemon type (e.g, fire, water, ground). Type "open log" if you want to access the log (More about that below).

Help on how to use log woth "open log" command:

When you type "open log", it will ask you for a log key. After you have looked at pokemon stats, at the bottom it will say something like:
(Pokemon name will be here) abilities logged at (Pokemon name will be here).abilities
(Pokemon name will be here) moves logged at (Pokemon name will be here).moves

So when it asks you for a log key, you can type, (Pokemon name will be here).moves, or, (Pokemon name will be here).abilities
