import io
import pygame
import requests
import tkinter as tk
from tkinter import messagebox
from tkinter import simpledialog
try:
    from PIL import Image, ImageTk
    import urllib.request
except ImportError:
    import os
    os.system("pip install urllib")
    os.system("pip install pillow")
    del os
    from PIL import Image, ImageTk
    import urllib.request


global error, pokemon, rt, rts, research
root = tk.Tk()
pygame.mixer.init()
error = pygame.mixer.Sound("error.wav")
item = ""
pokemon = pygame.mixer.Sound("pokemon.wav")
rts = ["Pokemon", "Type", "Move"]
rt = tk.StringVar(root)
rt.set("What do you want to research?")
research = tk.OptionMenu(root, rt, *rts)
research.pack(padx=45, pady=45)
def formatpokemondata(json):
    sheet = tk.Tk()
    sheet.title(json["name"].capitalize())
    # Window Setup \/
    titleimage = requests.get(f"https://pokeapi.co/api/v2/pokemon/{json['name']}").json()["sprites"]["front_default"]
    titleimage = urllib.request.urlopen(titleimage).read()
    titleimage = Image.open(io.BytesIO(titleimage))
    titleimage = ImageTk.PhotoImage(titleimage, master=sheet)
    root.images = []
    root.images.append(titleimage)
    label1 = tk.Label(sheet, image=titleimage)
    label1.grid(row=0, column=0)
    title = tk.Label(sheet, text=json["name"].capitalize(), font=['Arial', 30])
    title.grid(row=0, column=1, padx=5, pady=5)
    closebtn = tk.Button(sheet, text="Close", command=sheet.destroy, padx=5, pady=5)
    closebtn.grid(row=7, column=7, padx=5, pady=5)
    # Window Setup /\
    weight = tk.Label(sheet, text=f"{json['weight']} Pounds")
    weight.grid(row=1, column=0, padx=5, pady=5)
    height = tk.Label(sheet, text=f"{json['height']*10} Centimeters Tall")
    height.grid(row=1, column=2, padx=5, pady=5)

def researchsomething():
    if rt.get() == "Pokemon":
        thing = simpledialog.askstring("", "What Pokemon are you looking for?")
        item = requests.get(f"https://pokeapi.co/api/v2/pokemon/{thing}")
        if str(item) == "<Response [404]>":
            error.play()
            thing = messagebox.askyesno("", f'No Pokemon "{thing}" found, would you like to try again?')
            if thing == False:
                pass
            if thing == True:
                rt.set("Pokemon")
                researchsomething()
        else:
            data = item.json()
            formatpokemondata(data)


        
rbtn = tk.Button(root, text="Research", command=researchsomething)
rbtn.pack(padx=45, pady=45)
tk.mainloop()