import pygame

# Just a hashtag2
pygame.mixer.init()


def play_sound(path):
    my_sound = pygame.mixer.Sound(path)
    my_sound.play()
    pygame.time.wait(int(my_sound.get_length() * 1000))
